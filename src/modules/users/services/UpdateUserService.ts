import AppError from '@shared/errors/AppError';
import { getCustomRepository } from 'typeorm';
import User from '../typeorm/entities/Users';
import { UserRepository } from '../typeorm/repositories/UsersRepository';

interface IRequest {
  id: string;
  name: string;
  price: number;
  quantity: number;
}

class UpdateProductService {
  public async execute({}: IRequest): Promise<any> {
    const usersRepository = getCustomRepository(UserRepository);

    return null;
  }
}

export default UpdateProductService;
