import { Request, Response } from "express";
import CreateUserService from "../services/CreateUserService";
import ListUserService from "../services/ListUserService";
import ShowUserService from "../services/ShowUserService";

export default class UsersController {

  public async index(request: Request, response: Response): Promise<Response> {
    console.log(request.user.id);
    const listUsers = new ListUserService();
    console.log(listUsers);
    const users = await listUsers.execute();
    console.log(users);
    return response.json(users);
  }

  public async show(request: Request, response: Response): Promise<Response> {
    const { id } = request.params;
    const showUser = new ShowUserService();
    const user = await showUser.execute({ id });
    return response.json(user);
  }

  public async create(request: Request, response: Response): Promise<Response> {
    const { name, email, password } = request.body;
    const createUserService = new CreateUserService();
    const user = await createUserService.execute({ name, email, password })
    return response.json(user);
  }


}
